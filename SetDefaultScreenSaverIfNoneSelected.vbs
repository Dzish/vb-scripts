Option Explicit

On Error Resume Next

'Creating objects and variables


Const HKEY_CURRENT_USER = &H80000001

Dim WSHShell, RegKey, ScreenSaver, objReg, lastscreensaver

Set objReg = GetObject("winmgmts:\\.\root\default:StdRegProv")

RegKey = "HKEY_CURRENT_USER\Control Panel\Desktop\"

objReg.SetStringValue HKEY_CURRENT_USER,RegKey,"LastScreenSaver","None"

Set WSHShell = CreateObject("WScript.Shell")

'Checks registry for current screensaver

ScreenSaver = WSHShell.RegRead (regkey & "scrnsave.exe")

'A case statement to handle an error: Acts as an indicator for whether or not a screensaver is enabled.

Select Case Err

'If Screensaver is enabled, this case saves the current screensaver to
'the registry as the last used screensaver.
 
Case 0:

WSHShell.RegWrite regkey & "LastScreenSaver", ScreenSaver

'If Screensaver not enabled, this case checks to see if there's a "last used screensaver" stored. If so, it sets
'the screensaver to this value. If not, it sets the screensaver to the default XP Screensaver.

Case Else:

'Clears the error code.

Err.Clear()

'Checks to see if a "last used screen saver has been saved.

lastscreensaver = WSHShell.RegRead (regkey & "LastScreenSaver")

'Sets the screensaver to the last used screensaver.

WSHShell.RegWrite regkey & "scrnsave.exe", lastscreensaver

'This Select Case Structure handles an error that arises if no saved screensaver exists.

Select Case Err

Case 0:

'Don't do anything


Case Else:

'Sets the screensaver to the default XP screensaver...

WSHShell.RegWrite regkey & "scrnsave.exe", "C:\WINDOWS\System32\logon.scr"

'...then saves this as the last used screensaver so this error never arises again.

WSHShell.RegWrite regkey & "LastScreenSaver", "C:\WINDOWS\System32\logon.scr"

End Select

End Select

Err.Clear()

On Error Goto 0